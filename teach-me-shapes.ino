// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>
#include "math.h"

InternetButton button = InternetButton();
float beforeX;
float beforeY;
float beforeZ;

void setup() {
    
  button.begin();

  Particle.function("answer", showCorrectOrIncorrect);
  Particle.function("score", displayScore);
  Particle.function("turnOffLeds", turnOffLeds);

  // Show a visual indication to the player that the Particle
  // is loaded & ready to accept inputs
  for (int i = 0; i < 3; i++) {
    button.allLedsOn(130,20,40);
    delay(250);
    button.allLedsOff();
    delay(250);
  }
  
}

void loop() {
    
    // when person presses button send player choice to phone
    if (button.buttonOn(4)) {
        Particle.publish("playerChoice", "square", 60, PRIVATE);
    }
    
    if (button.buttonOn(3)) {
        Particle.publish("playerChoice", "triangle", 60, PRIVATE);
    } 
    
    if (button.buttonOn(1)) {
        Particle.publish("playerChoice", "score", 60, PRIVATE);
    }
    
    if (button.buttonOn(2)) {
        Particle.publish("playerChoice", "invalid", 60, PRIVATE);
    }
    // delay(200);


    // moving the particle
    // to calculate the angle, we only need the x and y
    // int xValue = button.readX();
    // if(beforeX != xValue){
    //     beforeX = xValue;
    //     Particle.publish("playerMove", "x" + String(xValue), 60, PRIVATE);    
    // }
    
    // int yValue = button.readY();
    // if(beforeY != yValue){
    //     beforeY = yValue;
    //     Particle.publish("playerMove", "y" + String(yValue), 60, PRIVATE);    
    // }
    
    // int zValue = button.readZ();
    // //This will make the color of the Button change with what direction you shake it
    // //The abs() part takes the absolute value, because negatives don't work well
    // button.allLedsOn(abs(xValue), abs(yValue), abs(zValue));
    delay(200);

}

int showCorrectOrIncorrect(String cmd) {

  if (cmd == "green") {
      
    button.allLedsOn(0,255,0);
    delay(2000);
    button.allLedsOff();
    
  } else if (cmd == "red") {
    
    button.allLedsOn(255,0,0);
    delay(2000);
    button.allLedsOff();
    
  } else {
    // you received an invalid color, so
    // return error code = -1
    return -1;
  }
  // function succesfully finished
  return 1;
}

int displayScore(String cmd) {
  // reset the displayed score
  button.allLedsOff();

  // turn on the specified number of lights
  int score = cmd.toInt();

  if (score < 0) {
    // error: becaues there are only 11 lights
    // return -1 means an error occurred
    return -1;
  }
  
  if (score >= 11) {
      button.allLedsOn(255, 255, 0);
  } else {
    for (int i = 1; i <= score; i++) {
      button.ledOn(i, 255, 255, 0);
    }
  }

  // return = 1 means the function finished running successfully
  return 1;
}

int turnOffLeds(String cmd)
{
    button.allLedsOff();
    return 1;
}