# TEACH ME SHAPES

* Giselle Cristina Moreno Tavares - C0744277

## Simulator/Device

	* iPhone 11 Pro Max
	* Particle

## Games

	* Mandatory game
	* Option B - Rotating a shape

### P.S:
Because the Option B there is no score, the score will appear for the user when s/he press the button 1 on the particle in the that game screen. However, on the Menu screen, there is a button for the **History Score**, that shows to the user all its score, fetching from Firebase database.
