//
//  DrawShapeViewController.swift
//  ParticleIOSStarter
//
//  Created by Giselle Tavares on 2019-11-03.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import Firebase
import Particle_SDK

class MoveShapeViewController: UIViewController {
    
    var ref: DatabaseReference! = Database.database().reference()
    
    // MARK: User variables
    let USERNAME = ProcessInfo.processInfo.environment["USERNAME"]
    let PASSWORD = ProcessInfo.processInfo.environment["PASSWORD"]
    
    // MARK: Device
    let DEVICE_ID = ProcessInfo.processInfo.environment["DEVICE_ID"]
    let DEVICE_NAME = ProcessInfo.processInfo.environment["DEVICE_NAME"]
    var myPhoton : ParticleDevice?
    
    // MARK: Variable for the game
    var arrayShapes = ["triangle", "square", "hexagon"]
    var shapeMove: String = ""

    @IBOutlet weak var imgShape: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // 1. Initialize the SDK
         ParticleCloud.init()

         // 2. Login to your account
         ParticleCloud.sharedInstance().login(withUser: self.USERNAME!, password: self.PASSWORD!) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription)
            }
            else {
                print("Login success!")
            }
         } // end login
        
         ParticleCloud.sharedInstance().getDevices { (devices:[ParticleDevice]?, error:Error?) -> Void in
            if let _ = error {
                print("Check your internet connectivity")
            }
            else {
                if let d = devices {
                    for device in d {
                        if device.name == self.DEVICE_NAME! {
                            self.myPhoton = device
                        }
                    }
                }
            }
         }
        
        // Show the shape
        let index = Int.random(in: 0 ..< arrayShapes.count)
        self.imgShape.image = UIImage(named: arrayShapes[index])
    }
    
    // MARK: Get Device from Cloud
    // Gets the device from the Particle Cloud
    // and sets the global device variable
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID!) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            } else {
                print("Got photon: \(device?.id)")
                self.myPhoton = device
            }
            
        }
        
    }
    
    //MARK: Subscribe to "playerMove" events on Particle
    func subscribeToParticleEvents() {
        var handler : Any?
        handler = ParticleCloud.sharedInstance().subscribeToDeviceEvents(
            withPrefix: "playerMove",
            deviceID: self.DEVICE_ID!,
            handler: {
                (event :ParticleEvent?, error : Error?) in
            
            if let _ = error {
                print("could not subscribe to events")
            } else {
                print("got event with data \(event?.data)")
                self.shapeMove = (event?.data)!
                
                var particleMoveX = self.imgShape.frame.origin.x
                var particleMoveY = self.imgShape.frame.origin.y
                
                if String(self.shapeMove.prefix(1)) == "x" {
                    print("x: \(self.shapeMove.dropFirst(1))")
                    particleMoveX = CGFloat(Double(self.shapeMove.dropFirst(1))!)
                }
                
                if String(self.shapeMove.prefix(1)) == "y" {
                    print("y: \(self.shapeMove.dropFirst(1))")
                    particleMoveY = CGFloat(Double(self.shapeMove.dropFirst(1))!)
                }
                
                let angle = atan2(particleMoveY, particleMoveX)
                print("angle: \(angle)")
        //        self.imgShape.transform = CGAffineTransform(rotationAngle: 0.34906585); // 20 degree
                self.imgShape.transform = CGAffineTransform(rotationAngle: angle); // 20 degree
                
            }
        })
    }

}
