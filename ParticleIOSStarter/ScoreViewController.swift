//
//  ScoreViewController.swift
//  ParticleIOSStarter
//
//  Created by Giselle Tavares on 2019-11-03.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import Firebase

class ScoreViewController: UIViewController {
    
    var ref: DatabaseReference! = Database.database().reference()

    @IBOutlet weak var txtScores: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.showScores()
    }
    
    func showScores(){
        var scoresArray = [String]()
        
        // fetching - because firebase is async, the result must be update inside it
        ref.child("scores").observe(.childAdded, with: { (score) in
            let result = score.value as? [String : AnyObject]
            print("result: \(result)")
            scoresArray.insert("\(result!["gameName"]!) - \(result!["shape"]!): \(result!["result"]!)", at: scoresArray.startIndex)
            
            var scoreList = ""
            for score in scoresArray {
                scoreList += "\(score)\n"
            }
            
            self.txtScores.text = scoreList
            
        })
    }
    

}
