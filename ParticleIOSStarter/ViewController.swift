//
//  ViewController.swift
//  ParticleIOSStarter
//
//  Created by Parrot on 2019-06-29.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import Particle_SDK

class ViewController: UIViewController {
    
    var storyBoard = UIStoryboard()
    
    @IBOutlet weak var howManySides: UIButton!
    @IBOutlet weak var moveShape: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.storyBoard = UIStoryboard(name: "Main", bundle:nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func howManySidesPressed(_ sender: UIButton) {
        
        let howManySidesViewController = self.storyBoard.instantiateViewController(withIdentifier: "howManySidesVC") as! HowManyShapesViewController
        self.present(howManySidesViewController, animated:true, completion:nil)
    }
    
    @IBAction func moveTheShapePressed(_ sender: UIButton) {
        
        let moveShapesViewController = self.storyBoard.instantiateViewController(withIdentifier: "moveShapeVC") as! MoveShapeViewController
        self.present(moveShapesViewController, animated:true, completion:nil)
    }
    
    
    
    @IBAction func randomGameButtonPressed(_ sender: UIButton) {
        let arrayGames = ["moveShapeVC", "howManySidesVC"]
        let index = Int.random(in: 0 ..< arrayGames.count)
        
        if arrayGames[index] == "moveShapeVC" {
            self.moveTheShapePressed(self.moveShape)
        } else {
            self.howManySidesPressed(self.howManySides)
        }
    }

}

