//
//  HowManyShapesViewController.swift
//  ParticleIOSStarter
//
//  Created by Giselle Tavares on 2019-11-03.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import Firebase
import Particle_SDK

class HowManyShapesViewController: UIViewController {
    
    var ref: DatabaseReference! = Database.database().reference()
    
    // MARK: User variables
    let USERNAME = ProcessInfo.processInfo.environment["USERNAME"]
    let PASSWORD = ProcessInfo.processInfo.environment["PASSWORD"]
    
    // MARK: Device
    let DEVICE_ID = ProcessInfo.processInfo.environment["DEVICE_ID"]
    let DEVICE_NAME = ProcessInfo.processInfo.environment["DEVICE_NAME"]
    var myPhoton : ParticleDevice?
    
    // MARK: Variable for the game
    var arrayShapes = ["triangle", "square"]
    var shapeShown: String = ""
    var gameId: String = ""
    var playerChoice: String = ""
    var resultGame: String = ""
    var correctAnswersNumber: Int = 0
    
    var isLoadingViewController = false

    @IBOutlet weak var imgShape: UIImageView!
    @IBOutlet weak var lblAnswer: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.resetValues()
    }
    
    func resetValues(){
        self.arrayShapes = ["triangle", "square"]
        self.shapeShown = ""
        self.playerChoice = ""
        self.resultGame = ""
        self.lblAnswer.text = "\(self.resultGame)"
        
        isLoadingViewController = true
        self.showAShape()
        self.initializeParticle()
    }
    
    func initializeParticle(){
        // 1. Initialize the SDK
        ParticleCloud.init()

        // 2. Login to your account
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME!, password: self.PASSWORD!) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription)
            } else {
                print("Login success!")

                // try to get the device
                self.getDeviceFromCloud()
            }
        } // end login
    }
    
    func showAShape(){
        // Show the shape
        let index = Int.random(in: 0 ..< self.arrayShapes.count)
            
        self.imgShape.image = UIImage(named: self.arrayShapes[index])
        self.shapeShown = self.arrayShapes[index]
        print("shapeShown: \(self.shapeShown)")
    }
    
    
    // MARK: Get Device from Cloud
    // Gets the device from the Particle Cloud
    // and sets the global device variable
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID!) { (device:ParticleDevice?, error:Error?) in
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            } else {
                print("Got photon from cloud: \(self.DEVICE_ID!)")
                self.myPhoton = device
                
                // subscribe to events
                self.subscribeToParticleEvents()
            }
            
        } // end getDevice()
    }
    
    //MARK: Subscribe to "playerChoice" events on Particle
    func subscribeToParticleEvents() {
        
        var handler : Any?
        handler = ParticleCloud.sharedInstance().subscribeToDeviceEvents(
            withPrefix: "playerChoice",
            deviceID: self.DEVICE_ID!,
            handler: {
                (event :ParticleEvent?, error : Error?) in
            
            if let _ = error {
                print("could not subscribe to events")
            } else {
                print("got event with data \(event!.data!)")
                self.playerChoice = (event!.data)!
                print("self.playerChoice: \(self.playerChoice)")
                
                print("self.shapeShown: \(self.shapeShown)")
                
                switch self.playerChoice {
                case self.shapeShown:
                    self.turnParticleGreen()
                case "score":
                    self.showScores()
                default:
                    self.turnParticleRed()
                    break;
                }
            }
        })
        
    }
    
    func saveScore(){
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddHHmmss"
        
        self.gameId = formatter.string(from: date)
        
        // save score on Firebase
        self.ref.child("scores").child(self.gameId).setValue(
            ["gameName": "How many sides",
             "shape": self.shapeShown,
             "answer": self.playerChoice,
             "result": self.resultGame]
        )
    }
    
    func showScores(){
        var countCorrectAnswers = 0
        
        // fetching - because firebase is async, the result must be update inside it. Getting the last 11 results because the number of leds on the particle
        ref.child("scores").queryLimited(toLast: 11).observeSingleEvent(of: .value) { (snap) in
            let results = snap.value as? [String : AnyObject]
            
            for result in (results?.values)! {
                print("result: \(result)")
                print("result: \(result["result"] as! String)")
                
                if result["result"] as! String == "CORRECT" {
                    countCorrectAnswers += 1
                }
            }
            self.correctAnswersNumber = countCorrectAnswers
            
            self.showAlertScore()
            self.showScoreParticle()
        }
    }
    
    func showAlertScore(){
        let alert = UIAlertController(title: "Score", message: "You got correctly \(self.correctAnswersNumber) answers", preferredStyle: .alert)

             let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
             })
             alert.addAction(ok)
             DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
        })
    }
    
    func turnParticleGreen() {
        self.resultGame = "CORRECT"
        self.saveScore()
        
        var task = myPhoton!.callFunction("answer", withArguments: ["green"]) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            
            DispatchQueue.main.async {
                print("self.resultGame: \(self.resultGame)")
                self.lblAnswer.text = "\(self.resultGame)"
            }
            
            if (error == nil) {
                print("Sent message to Particle to turn green")
            } else {
                print("Error when telling Particle to turn green")
            }
        }
        
    }
    
    func turnParticleRed() {
        self.resultGame = "INCORRECT"
        
        var task = myPhoton!.callFunction("answer", withArguments: ["red"]) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            
            DispatchQueue.main.async {
                print("self.resultGame: \(self.resultGame)")
                self.lblAnswer.text = "\(self.resultGame)"
            }
            
            if (error == nil) {
                print("Sent message to Particle to turn red")
            }
            else {
                print("Error when telling Particle to turn red")
            }
        }
    }
    
    func showScoreParticle() {
        var task = myPhoton!.callFunction("score", withArguments: ["\(self.correctAnswersNumber)"]) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to show the score")
            }
            else {
                print("Error when telling Particle to show the score")
            }
        }
    }
    
    @IBAction func playAgainButtonPressed(_ sender: UIButton) {
        self.resetValues()
    }
    
   

}
